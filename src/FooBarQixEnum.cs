using System;

namespace src
{
    internal enum Divisor
    {
        Foo = 3,
        Bar = 5,
        Qix = 7
    }

    public  class FooBarQixEnum : FooBarQix
    {
        public  string Process(int input)
        {
            var representation = ProcessDivisors(input) + ProcessContains(input);

            if (representation == "")
                return input.ToString();
            return representation;
        }

        private static string ProcessContains(int input)
        {
            var representation = "";
            foreach (var digit in input.ToString()) representation += MapDigitToRepresentation(digit);

            return representation;
        }

        private static string MapDigitToRepresentation(char digit)
        {
            var representation = "";

            foreach (var divisor in Divisors())
                if (DivisorValue(divisor).ToString() == digit.ToString())
                    representation += GetDivisorName(divisor);

            return representation;
        }

        private static int DivisorValue(object divisor)
        {
            return (int) divisor;
        }

        private static string ProcessDivisors(int input)
        {
            var representation = "";
            foreach (var divisor in Divisors())
                if (input % (int) divisor == 0)
                    representation += GetDivisorName(divisor);

            return representation;
        }

        private static Array Divisors()
        {
            return Enum.GetValues(typeof(Divisor));
        }

        private static string GetDivisorName(object divisor)
        {
            return Enum.GetName(typeof(Divisor), divisor);
        }
    }
}