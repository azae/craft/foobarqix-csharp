using System.Collections.Generic;

namespace src
{
    public class FooBarQixMap : FooBarQix
    {
        private static readonly Dictionary<string, int> divisors = new Dictionary<string, int>
        {
            {"Foo", 3},
            {"Bar", 5},
            {"Qix", 7}
        };

        public string Process(int input)
        {
            var representation = ProcessDivisors(input) + ProcessContains(input);
            if (representation == "")
                return input.ToString();
            return representation;
        }

        private static string ProcessContains(int input)
        {
            var representation = "";
            foreach (var digit in input.ToString()) representation += MapDigitToRepresentation(digit.ToString());
            return representation;
        }

        private static string MapDigitToRepresentation(string digit)
        {
            var representation = "";

            foreach (var divisor in divisors)
                if (divisor.Value.ToString() == digit)
                    representation += divisor.Key;

            return representation;
        }

        private static int DivisorValue(object divisor)
        {
            return (int) divisor;
        }

        private static string ProcessDivisors(int input)
        {
            var representation = "";
            foreach (var divisor in divisors)
                if (input % divisor.Value == 0)
                    representation += divisor.Key;

            return representation;
        }
    }
}