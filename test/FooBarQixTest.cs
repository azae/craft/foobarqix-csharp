using System;
using System.Collections;
using System.Collections.Generic;
using src;
using Xunit;

namespace test
{
    public class FooBarQixTest
    {
        [Theory]
        [ClassData(typeof(ImplList))]
        public void Should_return_input_as_string(FooBarQix fooBarQix)
        {
            Assert.Equal("1", fooBarQix.Process(1));
        }

        [Theory]
        [ClassData(typeof(ImplList))]
        public void ShouldReturnFooWhenDivisibleBy3(FooBarQix fooBarQix)
        {
            Assert.Equal("Foo", fooBarQix.Process(6));
        }

        [Theory]
        [ClassData(typeof(ImplList))]
        public void ShouldReturnBarWhenDivisibleBy5(FooBarQix fooBarQix)
        {
            Assert.Equal("Bar", fooBarQix.Process(10));
        }

        [Theory]
        [ClassData(typeof(ImplList))]
        public void ShouldReturnQixWhenDivisibleBy7(FooBarQix fooBarQix)
        {
            Assert.Equal("Qix", fooBarQix.Process(14));
        }

        [Theory]
        [ClassData(typeof(ImplList))]
        public void ShouldComposeDivisibleReplacement(FooBarQix fooBarQix)
        {
            Assert.Equal("FooBar", fooBarQix.Process(3 * 5 * 4));
            Assert.Equal("FooQix", fooBarQix.Process(3 * 7));
        }

        [Theory]
        [ClassData(typeof(ImplList))]
        public void ShouldReplace3ByFoo(FooBarQix fooBarQix)
        {
            Assert.Equal("Foo", fooBarQix.Process(31));
        }
    }

    public class ImplList : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] {new FooBarQixEnum()};
            yield return new object[] {new FooBarQixMap()};
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}