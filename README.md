# Démarrage rapide .Net core

    docker run -it --user $(id -u):$(id -g) -e HOME=/app -v $(pwd):/app mcr.microsoft.com/dotnet/core/sdk

Créer une nouvelle application

    dotnet new console src
    dotnet new xunit test

Ajouter coverlet pour la couverture de code, à faire dans le projet test

    cd test
    dotnet add package coverlet.collector
    dotnet add package coverlet.msbuild

Ajouter le formatage de code

    dotnet tool install -g dotnet-format

